![ |small ](imgs/jlab.png)
# Jupyter Lab launch script for midway

This repo contains a bash script (`launch-jlab.sh`) that when executed will start
a jupyter lab session on a midway compute node and present options for the user
to connect to it from their local web browser. The two options for connecting 
to the session from your local web browser depends on whether you are on the 
UChicago campus network or not. 

## Script User Parameters 

There are several parameters that the user can and should adjust at the top of
the `launch-jlab.sh` script that control the settings of the sbatch script used
to start the jupyter lab session on a midway compute node. The user adjustable
parameters are listed as follows: 

* PART           -- The midway partition name you intend to use (e.g. broadwl or caslake)

* TASKS          -- The number of tasks/cores to allocate to your notebook/lab

* TIME           -- The maximum wall time for the session

* ACCOUNT        -- PI account to use. If unset, your default slurm PI account is used.

* QOS            -- QOS to use with the specified partition. If unset the default is used.

* PYTHON_MODULE  -- The python/Anaconda module you intend to use. (e.g. python/anaconda-2020.02)

* CONDA_ENV      -- The conda environment to activate. If unset the base conda env is used

* CONSTRAINT     -- The slurm resource constraint. If unset, no constraint is applied. 

## Running the Script (Starting a notebook/lab session)
The easiest way to make use of the script is to clone this repo from midway2 
and copy the launch-nb.sh script to whichever directory you are working from. 

On midway: 
```
git clone https://git.rcc.uchicago.edu/jhskone/jupyter-lab/

cd jupyter-lab
cp launch-nb.sh <some directory>
```

After adjusting the header of the `launch-jlab.sh` script to specify the partition,
the number of cores, and the python module to use you can simply launch the script
by executing it from the command line as follows:  

```
./launch-jlab.sh
```

**Note:** *The script does NOT need to be run from within the repo folder. You can move it
to any location and run it.*

Upon executing the `launch-jlab.sh` script, resources will be requested on the partition
with the specified wall time and number of cores for your notebook session to run on. It may 
take some time to begin if there are not available resources in the partition you indicated 
to use. You can always check to see what resources are available on a specific partition
by using the `sinfo` command to check for "IDLE" resources. 

```
sinfo -p <partition-name>
```

where partition-name is the name of the partition (e.g. broadwl or broadwl-lc).

Once the resources have been allocated to your job, the jupyter lab server will
start on the compute resource and instructions for connecting to the session
will be printed to the command line along with the information about the jobid 
and instructions for terminating the session. 

## Connecting to the Juypter Lab Session from your Web Browser

Two sets of instructions are displayed to screen when the lab session begins. 
The first option is for those connecting from within the UChicago campus network.
If you are on campus or connected to UChicago network via VPN, then you should
choose this option. The alternative option is for those connecting from off campus 
and involves additional steps to setup the reverse tunnel to use your local web 
browser. 

## Ending a Lab Session

**NOTE** Closing the jupyter lab browser tab in your web browser does not stop 
the jupyter lab server session on the compute node. You can reconnect to it so 
long as the wall time has not been exhausted for the job. If you are finished 
using the Jupyter Lab session then you should cancel the launch job that is 
running on the midway compute node following the instructions that were printed 
to screen when the lab session was started. You need to use `scancel` along with 
the jobid to end the compute session. 

```
scancel <slurm-jobid>
```

Even if you are no longer using the lab notebook session, if you do not kill the
job your account will be charged for the remaining walltime. 

## Other Output

The launch script keeps a log of all the started sessions in a `session_logs`
folder of the current directory where you launched the script. Each session that
is labeled by its slurm jobid as `nb_session_<jobid>.log`. This contains all the 
lab server log information for the session. 



